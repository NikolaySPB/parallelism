﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Parallelism
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arrayCounters = new int[] { 100_000, 1_000_000, 10_000_000 };

            int numberTest = 1;

            for (int i = 0; i < arrayCounters.Length; i++)
            {
                Console.WriteLine($"\nTest № {numberTest}. Array size: {arrayCounters[i]}\n");

                int[] array = new int[arrayCounters[i]];

                FillingArray(array);

                TimeTest(() => SumArray(array));

                TimeTest(() => SumArrayParallel(array));

                TimeTest(() => SumArrayAsParallel(array));

                numberTest++;
            }

            Console.ReadLine();
        }

        static void FillingArray(int[] array)
        {
            Random random = new Random();

            for (int i = 0; i < array.Length; i++)            
                array[i] = random.Next(0, 100);            
        }

        static int SumArray(int[] array)
        {
            Console.Write("Work result no parallelism:\t");

            int sum = 0;

            for (int i = 0; i < array.Length; i++)            
                sum += array[i];
            
            return sum;
        }

        static int SumArrayParallel(int[] array)
        {
            Console.Write("Work result Parallel Threads:\t");

            int ProcessCount = Environment.ProcessorCount;

            int numberOfRanges = array.Length / ProcessCount;

            List<Thread> threads = new List<Thread>();

            int[] sumArray = new int[ProcessCount];

            for (int range = 0; range < ProcessCount; range++)
            {
                int rangeNumber = range;

                int start = rangeNumber * numberOfRanges;

                int finish = rangeNumber == ProcessCount - 1 ? array.Length : start + numberOfRanges;

                Thread thread = new Thread(() =>
                {
                    for (int i = start; i < finish; i++)                  
                        sumArray[rangeNumber] += array[i];                    
                });

                threads.Add(thread);                
            }

            threads.ForEach(x => x.Start());

            threads.ForEach(x => x.Join());

            return sumArray.Sum();
        }

        static int SumArrayAsParallel(int[] array)
        {
            Console.Write("Work result AsParallel:\t\t");

            return array.AsParallel().Sum();
        }

        static void TimeTest(Func<int> func)
        {
            Stopwatch stopwath = new Stopwatch();

            stopwath.Start();

            Console.Write($"Sum of array elements: {func.Invoke()}, ");

            stopwath.Stop();

            Console.Write($"Lead time: {stopwath.ElapsedMilliseconds} ms.\n");
        }
    }

}
